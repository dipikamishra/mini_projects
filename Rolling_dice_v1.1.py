# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 19:32:39 2018

@author: dipimish
"""

import random
import time

min = 1
max = 6

roll_again = "yes"

while roll_again == "yes" or roll_again == "y":
    
    print ("Rolling the dices...")
    time.sleep(2)
    print ("The values are....")
    time.sleep(1)
    print (random.randint(min, max))
    print (random.randint(min, max))

    roll_again = input("Roll the dices again?")
